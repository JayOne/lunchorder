require 'test_helper'

class MenuOrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @menu_order = menu_orders(:one)
  end

  test "should get index" do
    get menu_orders_url
    assert_response :success
  end

  test "should get new" do
    get new_menu_order_url
    assert_response :success
  end

  test "should create menu_order" do
    assert_difference('MenuOrder.count') do
      post menu_orders_url, params: { menu_order: { beatroot: @menu_order.beatroot, cabbage: @menu_order.cabbage, employee_id: @menu_order.employee_id, lettuce: @menu_order.lettuce, main_course: @menu_order.main_course, menu_id: @menu_order.menu_id, side1: @menu_order.side1, side2: @menu_order.side2, soup: @menu_order.soup } }
    end

    assert_redirected_to menu_order_url(MenuOrder.last)
  end

  test "should show menu_order" do
    get menu_order_url(@menu_order)
    assert_response :success
  end

  test "should get edit" do
    get edit_menu_order_url(@menu_order)
    assert_response :success
  end

  test "should update menu_order" do
    patch menu_order_url(@menu_order), params: { menu_order: { beatroot: @menu_order.beatroot, cabbage: @menu_order.cabbage, employee_id: @menu_order.employee_id, lettuce: @menu_order.lettuce, main_course: @menu_order.main_course, menu_id: @menu_order.menu_id, side1: @menu_order.side1, side2: @menu_order.side2, soup: @menu_order.soup } }
    assert_redirected_to menu_order_url(@menu_order)
  end

  test "should destroy menu_order" do
    assert_difference('MenuOrder.count', -1) do
      delete menu_order_url(@menu_order)
    end

    assert_redirected_to menu_orders_url
  end
end
