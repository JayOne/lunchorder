require "application_system_test_case"

class MenuOrdersTest < ApplicationSystemTestCase
  setup do
    @menu_order = menu_orders(:one)
  end

  test "visiting the index" do
    visit menu_orders_url
    assert_selector "h1", text: "Menu Orders"
  end

  test "creating a Menu order" do
    visit menu_orders_url
    click_on "New Menu Order"

    fill_in "Beatroot", with: @menu_order.beatroot
    fill_in "Cabbage", with: @menu_order.cabbage
    fill_in "Employee", with: @menu_order.employee_id
    fill_in "Lettuce", with: @menu_order.lettuce
    fill_in "Main course", with: @menu_order.main_course
    fill_in "Menu", with: @menu_order.menu_id
    fill_in "Side1", with: @menu_order.side1
    fill_in "Side2", with: @menu_order.side2
    fill_in "Soup", with: @menu_order.soup
    click_on "Create Menu order"

    assert_text "Menu order was successfully created"
    click_on "Back"
  end

  test "updating a Menu order" do
    visit menu_orders_url
    click_on "Edit", match: :first

    fill_in "Beatroot", with: @menu_order.beatroot
    fill_in "Cabbage", with: @menu_order.cabbage
    fill_in "Employee", with: @menu_order.employee_id
    fill_in "Lettuce", with: @menu_order.lettuce
    fill_in "Main course", with: @menu_order.main_course
    fill_in "Menu", with: @menu_order.menu_id
    fill_in "Side1", with: @menu_order.side1
    fill_in "Side2", with: @menu_order.side2
    fill_in "Soup", with: @menu_order.soup
    click_on "Update Menu order"

    assert_text "Menu order was successfully updated"
    click_on "Back"
  end

  test "destroying a Menu order" do
    visit menu_orders_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Menu order was successfully destroyed"
  end
end
