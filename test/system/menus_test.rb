require "application_system_test_case"

class MenusTest < ApplicationSystemTestCase
  setup do
    @menu = menus(:one)
  end

  test "visiting the index" do
    visit menus_url
    assert_selector "h1", text: "Menus"
  end

  test "creating a Menu" do
    visit menus_url
    click_on "New Menu"

    fill_in "Description", with: @menu.description
    fill_in "Due date", with: @menu.due_date
    fill_in "Main course", with: @menu.main_course
    fill_in "Side1", with: @menu.side1
    fill_in "Side2", with: @menu.side2
    fill_in "Soup", with: @menu.soup
    click_on "Create Menu"

    assert_text "Menu was successfully created"
    click_on "Back"
  end

  test "updating a Menu" do
    visit menus_url
    click_on "Edit", match: :first

    fill_in "Description", with: @menu.description
    fill_in "Due date", with: @menu.due_date
    fill_in "Main course", with: @menu.main_course
    fill_in "Side1", with: @menu.side1
    fill_in "Side2", with: @menu.side2
    fill_in "Soup", with: @menu.soup
    click_on "Update Menu"

    assert_text "Menu was successfully updated"
    click_on "Back"
  end

  test "destroying a Menu" do
    visit menus_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Menu was successfully destroyed"
  end
end
