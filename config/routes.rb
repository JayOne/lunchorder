Rails.application.routes.draw do
  resources :menu_orders
  devise_for :employees, controllers: { registrations: 'employees/registrations' }
  resources :menus
  resources :menu_orders do
    put :update_attribute, defaults: { format: 'js' }
  end

  get 'employees', to: 'views#employees'
  get 'calculation', to: 'views#calculation'
  get 'info', to: 'views#info'
  put 'views/update_employee', to: 'views#update_employee'
  delete 'views/delete_employee', to: 'views#delete_employee'

  root to: 'views#home'
end
