# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_27_191158) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "employees", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "full_name"
    t.boolean "admin", default: false
    t.boolean "approved", default: false
    t.index ["email"], name: "index_employees_on_email", unique: true
    t.index ["reset_password_token"], name: "index_employees_on_reset_password_token", unique: true
  end

  create_table "menu_orders", force: :cascade do |t|
    t.bigint "employee_id", null: false
    t.bigint "menu_id", null: false
    t.integer "soup", default: 0
    t.integer "main_course", default: 0
    t.integer "side1", default: 0
    t.integer "side2", default: 0
    t.integer "beetroot", default: 0
    t.integer "cabbage", default: 0
    t.integer "lettuce", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "bread", default: 0
    t.index ["employee_id"], name: "index_menu_orders_on_employee_id"
    t.index ["menu_id"], name: "index_menu_orders_on_menu_id"
  end

  create_table "menus", force: :cascade do |t|
    t.string "soup", default: "Nema"
    t.string "main_course", default: "Nema"
    t.string "side1", default: "Nema"
    t.string "side2", default: "Nema"
    t.datetime "due_date"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "soup_p", default: 0.0
    t.float "main_p", default: 0.0
    t.float "side1_p", default: 0.0
    t.float "side2_p", default: 0.0
    t.date "lunch_date"
    t.float "beetroot_p", default: 2.31
    t.float "cabbage_p", default: 2.31
    t.float "lettuce_p", default: 2.31
    t.float "bread_p", default: 0.5
  end

  add_foreign_key "menu_orders", "employees"
  add_foreign_key "menu_orders", "menus"
end
