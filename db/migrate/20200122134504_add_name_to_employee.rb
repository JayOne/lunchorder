class AddNameToEmployee < ActiveRecord::Migration[6.0]
  def change
    add_column :employees, :full_name, :string
  end
end
