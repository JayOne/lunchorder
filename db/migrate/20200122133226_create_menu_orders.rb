class CreateMenuOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :menu_orders do |t|
      t.references :employee, null: false, foreign_key: true
      t.references :menu, null: false, foreign_key: true
      t.integer :soup
      t.integer :main_course
      t.integer :side1
      t.integer :side2
      t.integer :beatroot
      t.integer :cabbage
      t.integer :lettuce

      t.timestamps
    end
  end
end
