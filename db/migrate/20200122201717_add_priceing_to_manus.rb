class AddPriceingToManus < ActiveRecord::Migration[6.0]
  def change
    add_column :menus, :soup_p, :float
    add_column :menus, :main_p, :float
    add_column :menus, :side1_p, :float
    add_column :menus, :side2_p, :float
  end
end
