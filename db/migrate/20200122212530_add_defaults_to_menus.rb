class AddDefaultsToMenus < ActiveRecord::Migration[6.0]
  def change
    change_column :menus, :main_course, :string, default: "Nema"
    change_column :menus, :side1, :string, default: "Nema"
    change_column :menus, :side2, :string, default: "Nema"
    change_column :menus, :soup, :string, default: "Nema"
    change_column :menus, :soup_p, :float, default: 0.0
    change_column :menus, :main_p, :float, default: 0.0
    change_column :menus, :side1_p, :float, default: 0.0
    change_column :menus, :side2_p, :float, default: 0.0
  end
end
