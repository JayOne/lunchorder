class CreateMenus < ActiveRecord::Migration[6.0]
  def change
    create_table :menus do |t|
      t.string :soup
      t.string :main_course
      t.string :side1
      t.string :side2
      t.datetime :due_date
      t.string :description

      t.timestamps
    end
  end
end
