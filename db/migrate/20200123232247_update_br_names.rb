class UpdateBrNames < ActiveRecord::Migration[6.0]
  def change
    rename_column :menu_orders, :beatroot, :beetroot
  end
end
