class AddPricesToMenus < ActiveRecord::Migration[6.0]
  def change
    add_column :menus, :beetroot_p, :float, default: 2.31
    add_column :menus, :cabbage_p, :float, default: 2.31
    add_column :menus, :lettuce_p, :float, default: 2.31
    add_column :menus, :bread_p, :float, default: 0.5
  end
end
