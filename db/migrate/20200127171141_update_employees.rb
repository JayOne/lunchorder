class UpdateEmployees < ActiveRecord::Migration[6.0]
  def change
    change_column :employees, :admin, :boolean, default: false
    add_column :employees, :approved, :boolean, default: false

  end
end
