class AddDefaultsToMenuOrders < ActiveRecord::Migration[6.0]
  def change
    change_column :menu_orders, :main_course, :integer, default: 0
    change_column :menu_orders, :side1, :integer, default: 0
    change_column :menu_orders, :side2, :integer, default: 0
    change_column :menu_orders, :soup, :integer, default: 0
    change_column :menu_orders, :beatroot, :integer, default: 0
    change_column :menu_orders, :cabbage, :integer, default: 0
    change_column :menu_orders, :lettuce, :integer, default: 0
    change_column :menu_orders, :bread, :integer, default: 0
  end
end
