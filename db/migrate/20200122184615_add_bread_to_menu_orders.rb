class AddBreadToMenuOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :menu_orders, :bread, :integer
  end
end
