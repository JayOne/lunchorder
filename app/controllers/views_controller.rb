class ViewsController < ApplicationController
  before_action :authorize_admin, only: [:home]
  before_action :authorize_employee, only: [:delete_employee, :employees, :update_employee]

  def home
    @menus=Menu.orderable.order(lunch_date: :asc)
  end
  def employees
    @employees=Employee.where(admin: false).order(approved: :asc)
  end

  def info
    todays_menu=Menu.where(lunch_date: Date.today).first
    @today=nil
    if !todays_menu.nil?
      @today=todays_menu.menu_orders.where(employee_id: current_employee.id).first
    end
    @month=current_employee.get_calculation(Date.today)
  end

  def delete_employee
    @employee=Employee.find(params[:employee_id])
    if @employee.destroy
      redirect_to views_employees_path
    end
  end

  def calculation
    @employees=Employee.where(admin:false).where(approved: true)
    if(params[:date])
      date=params[:date]
      @date=Date.new(date["year"].to_i,date["month"].to_i,date["day"].to_i)
    else
      @date=Date.current-1.months
    end

  end

  def update_employee
    @employee=Employee.find(params[:employee_id])
    if params[:update_param]=='Approve'
      @employee.update(approved: true)
    elsif params[:update_param]=='Disapprove'
      @employee.update(approved: false)
    end
    if @employee.save
      redirect_to views_employees_path
    end
  end
  protected
    def authorize_admin
      if current_employee.admin?
        redirect_to menus_url
      end
    end
    
    def authorize_employee
      if !current_employee.admin?
        redirect_to root_url
      end
    end
end
