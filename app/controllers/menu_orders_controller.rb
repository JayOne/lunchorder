class MenuOrdersController < ApplicationController
  before_action :set_menu_order, only: [:destroy]


  def create
    @menu_order = MenuOrder.new(menu_order_params)

    respond_to do |format|
      if @menu_order.save
        format.html { redirect_to @menu_order, notice: 'Menu order was successfully created.' }
        format.json { render :show, status: :created, location: @menu_order }
      else
        format.html { render :new }
        format.json { render json: @menu_order.errors, status: :unprocessable_entity }
      end
    end
  end



  # DELETE /menu_orders/1
  # DELETE /menu_orders/1.json
  def destroy
    @menu_order.destroy
    respond_to do |format|
      format.html { redirect_to menu_orders_url, notice: 'Menu order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def update_attribute
    @menu_order=MenuOrder.find(params[:menu_order_id])

    if params[:update_action]=="clear"
      @menu_order.set_defaults
    elsif params[:update_action]=="soup"
      @menu_order.update(soup: @menu_order.soup+1)
    elsif params[:update_action]=="main"
      @menu_order.update(main_course: @menu_order.main_course+1)
    elsif params[:update_action]=="side1"
      @menu_order.update(side1: @menu_order.side1+1)
    elsif params[:update_action]=="side2"
      @menu_order.update(side2: @menu_order.side2+1)
    elsif params[:update_action]=="beetroot"
      @menu_order.update(beetroot: @menu_order.beetroot+1)
    elsif params[:update_action]=="cabbage"
      @menu_order.update(cabbage: @menu_order.cabbage+1)
    elsif params[:update_action]=="lettuce"
      @menu_order.update(lettuce: @menu_order.lettuce+1)
    elsif params[:update_action]=="bread"
      @menu_order.update(bread: @menu_order.bread+1)
    end

    if @menu_order.save
      respond_to do |format|
        format.js
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu_order
      @menu_order = MenuOrder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_order_params
      params.require(:menu_order).permit(:employee_id, :menu_id, :soup, :main_course, :side1, :side2, :beetroot, :cabbage, :lettuce, :bread)
    end


end
