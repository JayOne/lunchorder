class MenusController < ApplicationController
  before_action :set_menu, only: [:show, :edit, :update, :destroy]
  before_action :authorize
  before_action :clean_passed
  # GET /menus
  # GET /menus.json
  def index
    @menus_active=Menu.active.order(lunch_date: :asc)
    @menus_passed=Menu.passed.order(lunch_date: :asc)
  end

  # GET /menus/1
  # GET /menus/1.json
  def show
    @menu_orders=@menu.menu_orders.ordered
  end

  # GET /menus/new
  def new
    @menu = Menu.new
  end

  # GET /menus/1/edit
  def edit
  end

  # POST /menus
  # POST /menus.json
  def create
    @menu = Menu.new(menu_params)
    @employees = Employee.where(admin: false)
    respond_to do |format|
      if @menu.save
        @employees.each do |e|
          MenuOrder.create(employee_id: e.id, menu_id: @menu.id, soup: 0, main_course: 0, side1: 0, side2: 0, beetroot: 0, cabbage: 0, lettuce: 0, bread: 0)
        end
        format.html { redirect_to menus_url, notice: 'Menu was successfully created.' }
        format.json { render :show, status: :created, location: @menu }
      else
        format.html { render :new }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /menus/1
  # PATCH/PUT /menus/1.json
  def update
    respond_to do |format|
      if @menu.update(menu_params)
        format.html { redirect_to @menu, notice: 'Menu was successfully updated.' }
        format.json { render :show, status: :ok, location: @menu }
      else
        format.html { render :edit }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menus/1
  # DELETE /menus/1.json
  def destroy
    @menu.destroy
    respond_to do |format|
      format.html { redirect_to menus_url, notice: 'Menu was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu
      @menu = Menu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_params
      params.require(:menu).permit(:soup, :soup_p, :main_course, :main_p, :side1, :side1_p, :side2, :side2_p, :lunch_date, :due_date, :description)
    end

    def authorize
      if not current_employee.admin?
        redirect_to root_url
      end
    end
    def clean_passed
      Menu.passed.destroy_all
    end
end
