class ApplicationController < ActionController::Base
    before_action :authenticate_employee!
    before_action :configure_permitted_parameters, if: :devise_controller?
    around_action :set_time_zone

    
    private
  
    def set_time_zone
      Time.use_zone('Europe/Zagreb') { yield }
    end
    
    protected

     def configure_permitted_parameters
          devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:full_name, :email, :password, :password_confirmation)}

          devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:full_name, :email, :password, :current_password)}
     end
     def after_sign_out_path_for(resource_or_scope)
          root_path
     end
     def after_update_path_for(resource_or_scope)
          root_path
     end
end
