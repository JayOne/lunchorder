json.extract! menu, :id, :soup, :main_course, :side1, :side2, :lunch_date, :side1_p, side2_p, :main_p, :soup_p, :due_date, :description, :created_at, :updated_at
json.url menu_url(menu, format: :json)
