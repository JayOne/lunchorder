class Employee < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_many :menu_orders, dependent: :destroy
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def active_for_authentication? 
    super && approved? 
  end        
  def get_calculation(date)
    menu_orders=self.menu_orders.joins(:menu).where('extract(year  from lunch_date) = ?', date.year).where('extract(month from lunch_date) = ?', date.month)
    calculation=0.0
    menu_orders.each do |mo|
      calculation+=mo.soup*mo.menu.soup_p + 
                  mo.main_course*mo.menu.main_p +
                  mo.side1*mo.menu.side1_p +
                  mo.side2*mo.menu.side2_p +
                  mo.beetroot*mo.menu.beetroot_p +
                  mo.lettuce*mo.menu.lettuce_p +
                  mo.cabbage*mo.menu.cabbage_p +
                  mo.bread*mo.menu.bread_p
    end
    return calculation.round(2)
  end

end
