class Menu < ApplicationRecord
    has_many :menu_orders, dependent: :destroy
    before_save :set_default
    scope :active, -> {where('lunch_date > ?', Date.today)}
    scope :orderable, -> {where('due_date > ?', DateTime.now)}
    scope :passed, -> {where('lunch_date < ?', Date.today)}
    validates :lunch_date, uniqueness: true

    protected
    def set_default
        self.soup = self.soup || "Nema"
        self.main_course = self.main_course || "Nema"
        self.side1 = self.side1 || "Nema"
        self.side2 = self.side2 || "Nema"
        if self.lunch_date.wday == 1
            d=self.lunch_date-3
        else
            d=self.lunch_date-1
        end
        
        self.due_date = Time.zone.local(d.year, d.month, d.day, 14, 0, 0)
    end

end
