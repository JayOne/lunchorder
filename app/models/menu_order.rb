class MenuOrder < ApplicationRecord
  belongs_to :employee
  belongs_to :menu
  scope :ordered, -> {where.not('soup=0 and main_course=0 and side1=0 and side2=0 and beetroot=0 and lettuce=0 and cabbage=0 and bread=0')}

  def set_defaults
    self.update(soup: 0, main_course: 0, side1: 0, side2: 0, beetroot: 0, cabbage: 0, lettuce: 0, bread: 0)
  end
  def not_ordered
    self.soup==0 and self.main_course==0 and self.side1==0 and self.side2==0 and self.beetroot==0 and self.lettuce==0 and self.cabbage==0 and self.bread==0
  end
end
